import { viteMockServe } from "vite-plugin-mock";
import { defineConfig, ConfigEnv } from 'vite'
import vue from '@vitejs/plugin-vue'
const { resolve } = require('path')
// https://vitejs.dev/config/
/** @type {import('vite').UserConfig} */
export default defineConfig({
  resolve: {
    extensions: ['.js','.vue','.json'],
    alias: {
      "@": resolve(__dirname, "./src"),
      "components": resolve(__dirname, "./src/components"),
      "styles": resolve(__dirname, "./src/styles"),
      "utils": resolve(__dirname, "./src/utils"),
    }
  },
  plugins: [
    vue(),
    viteMockServe({
      mockPath: 'mock',
      prodEnabled: ConfigEnv !== 'serve',
      injectCode: `
        import { setupProdMockServer } from './mockProdServer';
        setupProdMockServer();
      `,
    })]
})
